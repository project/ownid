<?php

namespace Drupal\ownid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @file
 * Contains \Drupal\ownid\Controller\OwnidController.
 */
/**
 * Handles multiple login functionalities.
 */
class OwnidController extends ControllerBase implements ContainerInjectionInterface {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->database = $container->get('database');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Sets ownid data by user login id.
   */
  public function setOwnIdDataByLoginId(Request $request) {
    $response = new JsonResponse();

    if (!$this->requestVerification($request)) {
      $response->setStatusCode(401);
      $response->setData(['errorCode' => 401]);

      return $response;
    }

    $data = json_decode($request->getContent(), TRUE);
    $user = $this->getUserFromRequest($request);

    if ($user instanceof UserInterface) {
      $connection = $this->database;

      try {
        $result = $connection->select('ownid_data', 'od')
          ->condition('od.uid', $user->id(), '=')
          ->fields('od', ['uid'])
          ->range(0, 1)
          ->execute()
          ->fetch();

        $dbRequest = $result->uid != $user->id()
                    ? $connection->insert('ownid_data')
                    : $connection->update('ownid_data')->condition('uid', $user->id(), '=');

        $dbRequest->fields(['data' => $data['ownIdData'], 'uid' => $user->id()])->execute();

        $response->setStatusCode(204);
      }
      catch (\Exception $e) {
        $response->setStatusCode(500);
        $response->setData(['error' => TRUE, 'message' => 'Unexpected error']);
      }
    }
    else {
      $response->setData(['errorCode' => 404, 'errorMessage' => 'User not found']);
    }

    return $response;
  }

  /**
   * Gets ownid data by user login id.
   */
  public function getOwnIdDataByLoginId(Request $request) {
    $response = new JsonResponse();

    if (!$this->requestVerification($request)) {
      $response->setStatusCode(401);
      $response->setData(['errorCode' => 401]);

      return $response;
    }

    $user = $this->getUserFromRequest($request);

    if ($user instanceof UserInterface) {
      $connection = $this->database;

      try {
        $result = $connection->select('ownid_data', 'od')
          ->condition('od.uid', $user->id(), '=')
          ->fields('od', ['data'])
          ->range(0, 1)
          ->execute()
          ->fetch();

        $response->setData(['ownIdData' => $result->data ?: ""]);
      }
      catch (\Exception $e) {
        $response->setStatusCode(500);
        $response->setData(['errorCode' => 500, 'errorMessage' => 'Unexpected error']);
      }
    }
    else {
      $response->setData(['errorCode' => 404, 'errorMessage' => 'User not found']);
    }

    return $response;
  }

  /**
   * Gets session data by user login id.
   */
  public function getSessionByLoginId(Request $request) {
    $response = new JsonResponse();

    if (!$this->requestVerification($request)) {
      $response->setStatusCode(401);
      $response->setData(['errorCode' => 401]);

      return $response;
    }

    $user = $this->getUserFromRequest($request);

    if ($user instanceof UserInterface) {
      $secret = $this->configFactory->get('ownid.settings')->get('ownid_secret');

      $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
      $payload = json_encode(['sub' => $user->id(), 'exp' => time() + 60]);

      $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
      $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

      $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, TRUE);

      $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

      $jwt = $base64UrlHeader . '.' . $base64UrlPayload . '.' . $base64UrlSignature;

      $response->setData(['token' => $jwt]);
    }
    else {
      $response->setData(['errorCode' => 404, 'errorMessage' => 'User not found']);
    }

    return $response;
  }

  /**
   * Handles exchange of tokens for login.
   */
  public function exchangeToken(Request $request) {
    $secret = $this->configFactory->get('ownid.settings')->get('ownid_secret');
    $response = new JsonResponse();
    $data = json_decode($request->getContent(), TRUE);

    $tokenParts = explode('.', $data['token']);
    $header = base64_decode($tokenParts[0]);
    $payload = base64_decode($tokenParts[1]);
    $signature_provided = $tokenParts[2];

    // Check the expiration time - note this will cause an error
    // if there is no 'exp' claim in the jwt.
    $expiration = json_decode($payload)->exp;
    $is_token_expired = ($expiration - time()) < 0;

    // Build a signature based on the header and payload using the secret.
    $base64_url_header = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
    $base64_url_payload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
    $signature = hash_hmac('SHA256', $base64_url_header . "." . $base64_url_payload, $secret, TRUE);
    $base64_url_signature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

    // If the JWT verification is correct.
    if (!$is_token_expired && ($base64_url_signature === $signature_provided)) {
      $uid = json_decode($payload)->sub;
      $account = $this->entityTypeManager->getStorage('user')->load($uid);

      user_login_finalize($account);

      $response->setData(
            $request->get('destination')
                ? ['reload' => TRUE, 'redirect' => $request->get('destination')]
                : ['reload' => TRUE]
        );
    }
    else {
      $response->setStatusCode(401);
      $response->setData(['errorCode' => 401, 'errorMessage' => 'Token is not valid']);
    }

    return $response;
  }

  /**
   * Stores users ownid data.
   */
  public static function onUserRegister($uid, $ownidData) {
    \Drupal::service('database')
      ->insert('ownid_data')
      ->fields(['data' => $ownidData, 'uid' => $uid])->execute();
  }

  /**
   * Gets users request data.
   */
  private function getUserFromRequest($request) {
    $data = json_decode($request->getContent(), TRUE);

    $result = $this->entityTypeManager
      ->getStorage('user')
      ->loadByProperties(['mail' => $data['loginId']]);
    return reset($result);
  }

  /**
   * Handles the ownid signature verification.
   */
  private function requestVerification($request) {
    if (!$request->headers->get('ownid-signature') || !$request->headers->get('ownid-timestamp')) {
      return FALSE;
    }

    $ownIDSig = $request->headers->get('ownid-signature');
    $ownIDTimestamp = $request->headers->get('ownid-timestamp');

    $secret = $this->configFactory->get('ownid.settings')->get('ownid_secret');
    $secretRaw = base64_decode($secret);

    $entityBody = file_get_contents('php://input');

    $stringToEncode = $entityBody . "." . $ownIDTimestamp;

    $sig = hash_hmac('sha256', $stringToEncode, $secretRaw);

    $sig = base64_encode(hex2bin($sig));

    return $sig === $ownIDSig;
  }

}
