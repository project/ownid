<?php

namespace Drupal\ownid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configures the ownid settings.
 *
 *    @package Drupal\ownid\Form
 */
class OwnidSettingsForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ownid_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ownid.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('ownid.settings');

    $form['ownid_app_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('App Id'),
      '#description' => $this->t('Application Id from https://console.ownid.com'),
      '#default_value' => $settings->get('ownid_app_id'),
    ];
    $form['ownid_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Shared Secret'),
      '#default_value' => $settings->get('ownid_secret'),
    ];

    $form['ownid_advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];

    $form['ownid_advanced']['ownid_non_prod_env'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use early access OwnID env (unstable)'),
      '#default_value' => $settings->get('ownid_non_prod_env'),
    ];

    $form['ownid_advanced']['ownid_selected_env'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#default_value' => $settings->get('ownid_selected_env'),
      '#options' => [
        'uat' => $this->t('UAT'),
        'prod-eu' => $this->t('PROD-EU'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="ownid_non_prod_env"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ownid.settings')
      ->set('ownid_app_id', $form_state->getValue('ownid_app_id'))
      ->set('ownid_secret', $form_state->getValue('ownid_secret'))
      ->set('ownid_env', $form_state->getValue('ownid_non_prod_env') ? $form_state->getValue('ownid_selected_env') : '')
      ->set('ownid_non_prod_env', $form_state->getValue('ownid_non_prod_env'))
      ->set('ownid_selected_env', $form_state->getValue('ownid_selected_env'))
      ->save();

    drupal_flush_all_caches();

    $this->messenger()->addMessage($this->t('Saved OwnID configuration.'));
  }

}
