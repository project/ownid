<?php

/**
 * @file
 * Enhances the functionalities of ownid module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\PasswordConfirm;
use Drupal\Core\Render\Markup;
use Drupal\ownid\Controller\OwnidController;

/**
 * Implements hook_form_alter().
 */
function ownid_form_alter(&$form, $form_state, $form_id) {
  $script = '';

  if ($form['#form_id'] == 'user_register_form') {
    $formId = 'ownid-register-form' . rand();
    $form['#attributes']['class'][] = $formId;
    $form['account']['mail']['#attributes']['class'][] = 'ownid-login-field';
    $form['account']['pass']['#process'][] = 'ownid_user_register_form_process_pass';
    $form['actions']['submit']['#submit'][] = 'ownid_user_register_submit_handler';
    $form['actions']['submit']['#attributes']['class'][] = 'ownid-user-register-submit';

    $form['account']['ownid_data'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['ownid-data-field'],
      ],
    ];

    $script = '
ownid("register", {
  onRegister: ({data}) => { document.querySelector(".' . $formId . ' .ownid-data-field").value = data },
  loginIdField: document.querySelector(".' . $formId . ' .ownid-login-field"),
  passwordField: document.querySelector(".' . $formId . ' .ownid-password-field"),
  confirmPasswordContainer: document.querySelector(".' . $formId . ' .ownid-confirm-password").parentElement,
});';
  }

  if ($form['#form_id'] == 'user_login_form') {
    $formId = 'ownid-login-form' . rand();
    $form['#attributes']['class'][] = $formId;
    $form['name']['#attributes']['class'][] = 'ownid-login-field';
    $form['pass']['#attributes']['class'][] = 'ownid-password-field';

    $script = '
ownid("login", {
  loginIdField: document.querySelector(".' . $formId . ' .ownid-login-field"),
  passwordField: document.querySelector(".' . $formId . ' .ownid-password-field"),
});';
  }

  if ($form['#form_id'] == 'user_pass') {
    $formId = 'ownid-pass-form' . rand();
    $form['#attributes']['class'][] = $formId;
    $form['name']['#attributes']['class'][] = 'ownid-pass-field';

    $script = '
ownid("recover", {
  loginIdField: document.querySelector(".' . $formId . ' .ownid-pass-field"),
  targetField: document.querySelector(".' . $formId . ' .ownid-pass-field"),
});';
  }

  if ($script) {
    $form['ownid'][] = [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#value' => Markup::create($script),
    ];
  }
}

/**
 * Manages the ownid user register form passwords.
 */
function ownid_user_register_form_process_pass(&$element, FormStateInterface $form_state, &$complete_form) {
  $element = PasswordConfirm::processPasswordConfirm($element, $form_state, $complete_form);
  $element['pass1']['#attributes']['class'][] = 'ownid-password-field';
  $element['pass2']['#attributes']['class'][] = 'ownid-confirm-password';

  return $element;
}

/**
 * Manages the ownid user register form data.
 */
function ownid_user_register_submit_handler(array $form, FormStateInterface $form_state) {
  if (!$form_state->getValue('ownid_data')) {
    return;
  }

  OwnidController::onUserRegister($form_state->getValue('uid'), $form_state->getValue('ownid_data'));
}

/**
 * Implements hook_page_attachments().
 */
function ownid_page_attachments(array &$attachments) {
  global $base_url;
  $appId = \Drupal::config('ownid.settings')->get('ownid_app_id');
  $env = \Drupal::config('ownid.settings')->get('ownid_env');
  $cdn = $env ? $env . '.ownid' : 'ownid';
  if ($env === 'prod-eu') {
    $cdn = 'ownid-eu';
  }
  $url = str_replace('http://', 'https://', $base_url);
  $isLoggedIn = \Drupal::currentUser()->isAuthenticated() ? 'true' : 'false';

  $script = "((o,w,n,i,d)=>{o[i]=o[i]||(async(...a)=>((o[i].q=o[i].q||[]).push(a),{error:null,data:null})),
(d=w.createElement(\"script\")).src='https://cdn." . $cdn . ".com/sdk/'+n,d.async=1,w.head.appendChild(d)})
(window,document,'" . $appId . "','ownid');
ownid('init', {
  passwordFieldBinding: true,
  checkSession: () => " . $isLoggedIn . ",
  onError: (error) => (new Drupal.Message()).add(error, { type: 'error' }),
  language: '" . \Drupal::languageManager()->getCurrentLanguage()->getId() . "',
  onLogin: async function ({token}) {
    const response = await fetch('" . $url . "/ownid/exchangeToken', {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({token}),
    });
    const result = await response.json();
    if (result.reload) {
        location.reload();
    }
  }
});";
  $attachments['#attached']['html_head'][] = [
    [
      '#tag' => 'script',
      '#value' => Markup::create($script),
      '#weight' => -1,
    ],
    'ownid-loader-script',
  ];
}
