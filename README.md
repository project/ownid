# OwnID Passwordless Login

An easy and secure passwordless add-on for your website.

OwnID is a passwordless add-on for your Drupal website. The authentication flow is web-based and doesn't require you to install an app. Our complete end-to-end platform works cross-OS, cross-device, and cross-domain. It supports Passkeys out of the box.

Our plugin handles a multitude of login scenarios: new user login, existing user login, phone unavailable, biometrics not supported, and more. Your users will be able to log in quickly and easily, always.


## Requirements

Currently, our module only supports email as a login identifier.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration
1. Sign up to [OwnID console](https://console.ownid.com/registration?utm_source=drupal).
2. Select Drupal integration and complete your onboarding.
3. Copy and paste the AppID and Shared Secret into OwnID Drupal module Settings.
4. Enjoy your passwordless login! 🚀
